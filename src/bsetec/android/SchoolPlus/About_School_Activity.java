package bsetec.android.SchoolPlus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import bsetec.android.SchoolPlus.utils.Connection_Detector;
import bsetec.android.SchoolPlus.utils.Constants;
import bsetec.android.SchoolPlus.utils.Gallery_Folder_Files;

@SuppressWarnings("deprecation")
public class About_School_Activity extends ActionBarActivity {

	TextView contents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_aboutschool_);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#035A87")));
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(R.layout.actionbar);

		String fontPath = "asap_bold.otf";
		TextView tv = (TextView) findViewById(R.id.actionbar_text);
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		tv.setTypeface(tf);
		tv.setText("ABOUT SCHOOL");
		ImageButton ib = (ImageButton) findViewById(R.id.back_btn);
		ib.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				finish();
			}
		});

		contents = (TextView) findViewById(R.id.line1);

		new About_School_WS(About_School_Activity.this).execute();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public class About_School_WS extends AsyncTask<Void, Void, String> {
		InputStream in = null;
		InputStream is = null;
		String result, sub_folders, folder_names;
		String exam_type, exam_name, exam_id;
		ArrayList<Gallery_Folder_Files> files_array;
		private Context context;
		Connection_Detector cd;
		Boolean isInternetPresent = false;
		AlertDialog.Builder internet_alert_dialog;
		ArrayList<ArrayList<Gallery_Folder_Files>> folder_names_array;
		ProgressDialog dialog;
		GridView gv;

		public About_School_WS(Context context) {
			this.context = context;
			dialog = new ProgressDialog(context);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// folder_names_array = new
			// ArrayList<ArrayList<Gallery_Folder_Files>>();

			cd = new Connection_Detector(context);
			// dialog = new ProgressDialog(context);
			isInternetPresent = cd.isConnectingToInternet();
			if (!isInternetPresent) {

				internet_alert_dialog = new AlertDialog.Builder(context);
				internet_alert_dialog.setTitle("Internet Connectivity !");
				internet_alert_dialog
						.setMessage("Please ensure that you have connected to the internet and try again.");
				internet_alert_dialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								/*
								 * Intent back = new Intent(context,
								 * Menu_One_Fragment.class);
								 * context.startActivity(back);
								 */

							}
						});
				internet_alert_dialog.show();
				this.cancel(true);
			}

			dialog.setMessage("Loading....");
			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) { // TODO Auto-generated
															// method stub
			try {
				HttpClient httpclient = new DefaultHttpClient();

				HttpPost httppost = new HttpPost(Constants.WEBSERVICE_URL
						+ "exam/getaboutschool");

				// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				System.out.println("httppost "
						+ httppost.getURI().toURL().toString());
				HttpResponse response = httpclient.execute(httppost);

				HttpEntity entity = response.getEntity();

				is = entity.getContent();

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
			} catch (Exception e) {

			}

			return result;

		}

		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			dialog.dismiss();
			String thumbnail = null;

			try {
				SharedPreferences preferences = PreferenceManager
						.getDefaultSharedPreferences(context);
				SharedPreferences.Editor editor = preferences.edit();
				JSONObject jObj = new JSONObject(result);

				String status = jObj.getString("status");

				String aboutschString = jObj.getJSONArray("about_school")
						.getJSONObject(0).getString("about_school");
				System.out.println("STATUS" + aboutschString);
				contents.setText(aboutschString);

			}

			catch (JSONException e) {

				e.printStackTrace();

			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

}
